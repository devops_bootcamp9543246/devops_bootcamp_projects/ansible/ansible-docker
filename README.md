# 15 - Configuration Management with Ansible - Ansible & Docker

**Demo Project:**
Ansible & Docker

**Technologies used:**
Ansible, AWS, Docker, Terraform, Linux

**Project Description:**
- Create AWS EC2 Instance with Terraform
- Write Ansible Playbook that installs necessary technologies like Docker and Docker Compose, copies docker-compose file to the server and starts the Docker   containers configured inside the docker- compose file



15 - Configuration Management with Ansible
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp
